<?php
$answer = array(
    1 => "A. CSS",
    2 => "B. color",
    3 => "B. SGML",
    4 => "D. All of the mentioned",
    5 => "C. target=”_blank”",
    6 => "A. CSS",
    7 => "B. color",
    8 => "B. SGML",
    9 => "D. All of the mentioned",
    10 => "C. target=”_blank”",
);

$score = 0;
for ($i = 1; $i <= 10; $i++) {
    if ($_COOKIE["cau" . $i . ""] == $answer[$i]) {
        $score++;
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Result</title>
    <style>
        .wrapper {
            display: grid;
            justify-content: center;
        }

        .result {
            padding: 30px 60px;
            border-radius: 20px;
            text-align: center;
            font-size: 20px;
            margin-top: 50px;
            margin-bottom: 10px;
            color: white;
            width: 300px;
            place-self: center;
        }

        .normal {
            background: #dfa400;
        }

        .bad {
            background: #d91e00;
        }

        .good {
            background: #00a30c;
        }

        .correct {
            color: #0c7200;
            margin-bottom: 5px;
        }

        .wrong {
            color: #f52200;
            margin-bottom: 10px;
        }
    </style>
</head>

<body>
    <div class="wrapper">
        <?php
        if ($score < 4) {
            echo '  <div class="result bad">
                                <b class="score">Điểm của bạn là: <span style="color: #86ef8e">' . $score . '</span></b> <br>
                                <b class="content">Bạn quá kém cần ôn tập thêm!</span>
                            </div>
                    ';
        } else if ($score > 4 && $score < 7) {
            echo '  <div class="result normal">
                                <b class="score">Điểm của bạn là: <span style="color: red">' . $score . '</span></b> <br>
                                <b class="content">Cũng bình thường!</b>
                            </div>
                    ';
        } else {
            echo '  <div class="result good">
                                <b class="score">Điểm của bạn là: <span style="color: red">' . $score . '</span></b> <br>
                                <b class="content">Sắp sửa làm trợ giảng lớp PHP!</b>
                            </div>
                    ';
        }
        ?>
        <h3 style="margin-bottom: 10px">Kết quả chi tiết:</h3>
        <div class="result-detail">
            <?php
            for ($i = 1; $i <= 10; $i++) {

                if ($_COOKIE["cau" . $i . ""] != $answer[$i]) {
                    echo '<span class="wrong"><b style="color: black">Câu ' . $i . ':   </b>' . $_COOKIE["cau" . $i . ""] . '</span>';
                } else {
                    echo '<span class="correct"><b style="color: black">Câu ' . $i . ':   </b>' . $answer[$i] . '</span>';
                }
                echo '<div style="margin-bottom: 10px">Đáp án: <span class="correct">' . $answer[$i] . '</span></div>';
            }
            ?>
        </div>


    </div>
</body>

</html>