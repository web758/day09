<?php

$list_questions = array(
    1 => array(
        "qus" => "Which element is used for or styling HTML5 layout?",
        "A" => "CSS", 
        "B" => "jQuery",
        "C" =>  "JavaScript", 
        "D" => "PHP"
    ),
    2 => array(
        "qus" => "Which of the following is not the element associated with the HTML table layout?",
        "A" => "alignment",
        "B" =>  "color",
        "C" => "size",
        "D" => "spanning"
    ),
    3 => array(
        "qus" => "HTML is a subset of ___________",
        "A" => "SGMT", 
        "B" => "SGML", 
        "C" => "SGME", 
        "D" => "XHTML"
    ),
    4 => array(
        "qus" => "What is HTML?",
        "A" => "HTML describes the structure of a webpage",
        "B" => "HTML is the standard markup language mainly used to create web pages",
        "C" => "HTML consists of a set of elements that helps the browser how to view the content",
        "D" => "All of the mentioned"
    ),
    5 => array(
        "qus" => "In HTML, which attribute is used to create a link that opens in a new window tab?",
        "A" => "src=”_blank”", 
        "B" => "alt=”_blank”", 
        "C" => "target=”_blank”",
        "D" => "target=”_self”"
    ),
    6 => array(
        "qus" => "Which element is used for or styling HTML5 layout?",
        "A" => "CSS", 
        "B" => "jQuery",
        "C" =>  "JavaScript", 
        "D" => "PHP"
    ),
    7 => array(
        "qus" => "Which of the following is not the element associated with the HTML table layout?",
        "A" => "alignment",
        "B" =>  "color",
        "C" => "size",
        "D" => "spanning"
    ),
    8 => array(
        "qus" => "HTML is a subset of ___________",
        "A" => "SGMT", 
        "B" => "SGML", 
        "C" => "SGME", 
        "D" => "XHTML"
    ),
    9 => array(
        "qus" => "What is HTML?",
        "A" => "HTML describes the structure of a webpage",
        "B" => "HTML is the standard markup language mainly used to create web pages",
        "C" => "HTML consists of a set of elements that helps the browser how to view the content",
        "D" => "All of the mentioned"
    ),
    10 => array(
        "qus" => "In HTML, which attribute is used to create a link that opens in a new window tab?",
        "A" => "src=”_blank”", 
        "B" => "alt=”_blank”", 
        "C" => "target=”_blank”",
        "D" => "target=”_self”"
    ),
);


$cookie_duration = strtotime('+1 days');
for ($i = 1; $i <= 10; $i++) {
    if (isset($_POST["cau" . $i . ""])) {
        setcookie("cau" . $i . "", $_POST["cau" . $i . ""] . ". " . $list_questions[$i][$_POST["cau" . $i . ""]] . "", $cookie_duration, "/");
    }
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    header("Refresh:0; url=Result.php");
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Excercise</title>
    <style>
        body {
            padding: 50px;
        }

        h1 {
            margin: 0px;
        }

        .wrapper {
            padding: 50px 80px;
            background-color: #dfdfdf;
            border-radius: 50px;
        }

        .btn-next, 
        .btn-back,
        .btn-submit {
            margin-top: 20px;
            margin-right: 10px;
            padding: 8px 20px;
            background: #2587ff;
            color: white;
            border: 0px;
            border-radius: 5px;
        }

        .btn-submit{
            background: #ff8100;
        }

        .page2 {
            display: none;
        }

        .label_question {
            font-weight: bold;
        }

        .question_item {
            margin-bottom: 20px;
        }
    </style>
</head>

<body>

    <div class="wrapper">
        <h1>Quick Test HTML</h1>
        <form action="" method="POST">
            <div class="page1" id="page1">
                <h3 class="page_num">Page 1/2</h3>
                <?php
                for ($i = 1; $i <= 5; $i++) {
                    echo '  <div class="question_item">
                                <label class="label_question">Câu ' . $i . ' : ' . $list_questions[$i]["qus"] . ' </label> <br>
                                <input type="radio" name="cau' . $i . '" value="A">' . $list_questions[$i]["A"] . '<br>
                                <input type="radio" name="cau' . $i . '" value="B">' . $list_questions[$i]["B"] . '<br>
                                <input type="radio" name="cau' . $i . '" value="C">' . $list_questions[$i]["C"] . '<br>
                                <input type="radio" name="cau' . $i . '" value="D">' . $list_questions[$i]["D"] . '<br> 
                            </div>';
                };
                ?>
                <button class="btn-next" id="btn-next" type="button">Next page</button>
            </div>

            <div class="page2" id="page2">
                <h3 class="page_num">Page 2/2</h3>
                <?php
                for ($i = 6; $i <= 10; $i++) {
                    echo '  <div class="question_item">
                                <label class="label_question">Câu ' . $i . ' : ' . $list_questions[$i]["qus"] . ' </label> <br>
                                <input type="radio" name="cau' . $i . '" value="A">' . $list_questions[$i]["A"] . '<br>
                                <input type="radio" name="cau' . $i . '" value="B">' . $list_questions[$i]["B"] . '<br>
                                <input type="radio" name="cau' . $i . '" value="C">' . $list_questions[$i]["C"] . '<br>
                                <input type="radio" name="cau' . $i . '" value="D">' . $list_questions[$i]["D"] . '<br> 
                            </div>';
                };
                ?>
                <button class="btn-back" type="button" id="btn-back">Back page</button>
                <button class="btn-submit" type="submit" id="btn-submit">Submit</button>
            </div>

            <script>
                var btnSubmit = document.getElementById("btn-submit")
                var btnNext = document.getElementById("btn-next")
                var btnBack = document.getElementById("btn-back")
                btnNext.onclick = function() {
                    document.getElementById("page1").style.display = "none"
                    document.getElementById("page2").style.display = "contents"
                }
                btnBack.onclick = function() {
                    document.getElementById("page2").style.display = "none"
                    document.getElementById("page1").style.display = "contents"
                }
            </script>

        </form>
    </div>
</body>

</html>